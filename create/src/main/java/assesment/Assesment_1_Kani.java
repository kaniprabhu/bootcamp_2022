package assesment;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Assesment_1_Kani {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(co);
		Actions action = new Actions(driver);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		
			//1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
			//2. Click on toggle menu button from the left corner
		executor.executeScript("arguments[0].click();", driver.findElement(By.className("slds-icon-waffle")));
			//3. Click view All 
		executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//lightning-button/button[@type='button']")));
			//4. Click Service Console from App Launcher
		List<WebElement> appLaunlist =	driver.findElements(By.xpath("//div[@class='slds-truncate']/a/following::p"));
		for(int i=0;i<appLaunlist.size();i++)
		{
			//System.out.println(appLaunlist.get(i).getText());
			if(appLaunlist.get(i).getAttribute("title").contains("(Lightning Experience) Lets sales reps work with multiple records on one screen"))
			{
				appLaunlist.get(i).click();
				break;
			}
		}
			//5. Select Home from the DropDown
		action.moveToElement(driver.findElement(By.xpath("//div[@class='slds-context-bar__icon-action']"))).click().build().perform();
		List<WebElement> Menudropdwonlist =	driver.findElements(By.xpath("//section[@role='dialog']/div/div/ul/li/div/a"));
		for(int i=0;i<Menudropdwonlist.size();i++)
		{
			//System.out.println(appLaunlist.get(i).getText());
			if(Menudropdwonlist.get(i).getAttribute("data-itemid").contains("Home"))
			{
				action.moveToElement(Menudropdwonlist.get(i)).click().build().perform();
				break;
			}
		}
		
			//7. Select Dashboards from DropDown
		Thread.sleep(5000);
		action.moveToElement(driver.findElement(By.xpath("//div[@class='slds-context-bar__icon-action']"))).click().build().perform();
		List<WebElement> Menudropdwonlist1 =	driver.findElements(By.xpath("//section[@role='dialog']/div/div/ul/li/div/a"));
		for(int i=0;i<Menudropdwonlist.size();i++)
		{
			//System.out.println(appLaunlist.get(i).getText());
			if(Menudropdwonlist1.get(i).getAttribute("data-itemid").contains("Dashboard"))
			{
				action.moveToElement(Menudropdwonlist1.get(i)).click().build().perform();
				break;
			}
		}
		
		Thread.sleep(7000);
			//8. Click on New Dashboard
		//action.moveToElement(driver.findElement(By.xpath("//div[@title='New Dashboard']"))).build().perform();
		executor.executeScript("arguments[0].click();",driver.findElement(By.xpath("//div[@title='New Dashboard']")));
			//9. Enter the Dashboard name as "YourName_Workout"
		Thread.sleep(7000);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		System.out.println("entered into frame");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dashboardNameInput")));
		driver.findElement(By.id("dashboardNameInput")).sendKeys("Kani Workout");
			//10. Enter Description as Testing and Click on Create
		driver.findElement(By.id("dashboardDescriptionInput")).sendKeys("Testing");
		driver.findElement(By.id("submitBtn")).click();
			//11. Click on Done
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/button[text()='Done']")));
		executor.executeScript("arguments[0].click();",driver.findElement(By.xpath("//div/button[text()='Done']")));
		
			//12. Verify the Dashboard is Created
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));		
		if(driver.findElement(By.xpath("//h1/span[@title='Kani Workout']")).getText().equals("Kani Workout"))
		{
			System.out.println("Dashboard is Created" );
		}
		
			//13. Click on Subscribe
		Thread.sleep(5000);
		executor.executeScript("arguments[0].click();",driver.findElement(By.xpath("//button[text()='Subscribe']")));
		
			//14. Select Frequency as "Daily"
		driver.switchTo().defaultContent();
		Thread.sleep(4000);
		action.moveToElement(driver.findElement(By.xpath("//label/span[text()='Daily']"))).click().build().perform();
		
			//15. Time as 10:00 AM
		Select dropddowntime = new Select(driver.findElement(By.id("time")));
		dropddowntime.selectByVisibleText("10:00 AM");
			//16. Click on Save
		action.moveToElement(driver.findElement(By.xpath("//button/span[text()='Save']"))).click().build().perform();
			//17. Verify "You started Dashboard Subscription" message displayed or not
		String sub_message = driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
		
	if(sub_message.equalsIgnoreCase("You started a dashboard subscription."))
	{
		System.out.println("Dashboards should be subscribed Successfully");
	}else if(sub_message.equalsIgnoreCase("Your subscription is all set."))
	{
		System.out.println("Dashboards Already  subscribed ");
	}
			//18. Close the "YourName_Workout" tab
	action.moveToElement(driver.findElement(By.xpath("//a[@role='tab']//following-sibling::div"))).click().build().perform();
			//19. Click on Private Dashboards
	action.moveToElement(driver.findElement(By.xpath("//li/a[text()='Private Dashboards']"))).click().build().perform();
			//20. Verify the newly created Dashboard available
	Thread.sleep(6000);
	driver.findElement(By.xpath("//input[@placeholder='Search recent dashboards...']")).sendKeys("KANI"+Keys.ENTER);
			//21. Click on dropdown for the item
	Thread.sleep(5000);
	wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table[@lightning-datatable_table]/tbody/tr/th//lightning-formatted-url/a")));
			//22. Select Delete
	List<WebElement> DashboardnameList =driver.findElements(By.xpath("//table[@lightning-datatable_table]/tbody/tr/th//lightning-formatted-url/a"));
	for(int i=0; i<DashboardnameList.size();i++)
	{
		if(DashboardnameList.get(i).getAttribute("title").equalsIgnoreCase("Salesforce Automation by KANI PRABHU S"))
		{
		//js.executeScript("arguments[0].click()", driver.findElement(By.xpath("//table[@lightning-datatable_table]/tbody/tr[1]/td[6]//span/div")));
			
		action.moveToElement(driver.findElement(By.xpath("//table[@lightning-datatable_table]/tbody/tr[1]/td[6]//span/div"))).click().build().perform();
		action.moveToElement(driver.findElement(By.xpath("//a/span[@class='slds-truncate'][text()='Delete']"))).click().build().perform();
		break;
		}
	}
			//23. Confirm the Delete
	action.moveToElement(driver.findElement(By.xpath("//button[@title='Delete']"))).click().build().perform();
			
	}

}
