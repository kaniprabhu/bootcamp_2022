package S06_Sprint_1;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteDashboard {

	public static void main(String[] args) throws InterruptedException {
		
			WebDriverManager.chromedriver().setup();
			ChromeOptions co=new ChromeOptions();
			co.addArguments("--disable-notifications");
			WebDriver driver = new ChromeDriver(co);
			WebDriverWait wait= new WebDriverWait(driver, Duration.ofSeconds(15));
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
			JavascriptExecutor js = (JavascriptExecutor)driver;
			Actions action=new Actions(driver);
			driver.manage().window().maximize();
			
			//1. Login to https://login.salesforce.com
			driver.get("https://login.salesforce.com/");
			driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
			driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
			driver.findElement(By.id("Login")).click();
			
			//2. Click on the toggle menu button from the left corner
			//js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")));
			action.moveToElement(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))).click().build().perform();
			
			//3. Click View All and click Dashboards from App Launcher
			js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//button[@class='slds-button']")));
			
			//4. Click on the Dashboards tab 
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span/p[text()='Dashboards']"))));
			action.moveToElement(driver.findElement(By.xpath("//span/p[text()='Dashboards']"))).click().build().perform();
			List<WebElement> MenuList = driver.findElements(By.xpath("//one-app-nav-bar-item-root/a"));
			for(int i=0; i<MenuList.size();i++)
			{
				if(MenuList.get(i).getAttribute("title").equalsIgnoreCase("Dashboards"))
				{
					action.moveToElement(MenuList.get(i)).click().build().perform();
					//System.out.println("Menu clicked");
					break;
				}
			}
			
			
			//5. Search the Dashboard 'Salesforce Automation by Your Name'
			driver.findElement(By.xpath("//input[@placeholder='Search recent dashboards...']")).sendKeys("KANI"+Keys.ENTER);
			
			//6. Click on the Dropdown icon and Select Delete
			Thread.sleep(5000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table[@lightning-datatable_table]/tbody/tr/th//lightning-formatted-url/a")));
		List<WebElement> DashboardnameList =driver.findElements(By.xpath("//table[@lightning-datatable_table]/tbody/tr/th//lightning-formatted-url/a"));
		for(int i=0; i<DashboardnameList.size();i++)
		{
			if(DashboardnameList.get(i).getAttribute("title").equalsIgnoreCase("Salesforce Automation by KANI PRABHU S"))
			{
			//js.executeScript("arguments[0].click()", driver.findElement(By.xpath("//table[@lightning-datatable_table]/tbody/tr[1]/td[6]//span/div")));
				
			action.moveToElement(driver.findElement(By.xpath("//table[@lightning-datatable_table]/tbody/tr[1]/td[6]//span/div"))).click().build().perform();
			action.moveToElement(driver.findElement(By.xpath("//a/span[@class='slds-truncate'][text()='Delete']"))).click().build().perform();
			break;
			}
		}
			
			//7.Click on the Delete option in the displayed popup window.
		action.moveToElement(driver.findElement(By.xpath("//button[@title='Delete']"))).click().build().perform();
			//8. Verify Whether Dashboard is Deleted using Dashboard Name
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']"))));
		//System.out.println(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText());
		String message = driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
			//Expected Result:Dashboards should be subscribed Successfully
		//System.out.println(message);
		if(message.equalsIgnoreCase("Dashboard was deleted."))
		{
			System.out.println("Dashboards Deleted Successfully");
		}

	}

}
