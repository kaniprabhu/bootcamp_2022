package S06_Sprint_1;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteCase {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(co);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4000));
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(2));
		
			//1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
			//2. Click on toggle menu button from the left corner
		//driver.findElement(By.className("//div[@class='slds-icon-waffle']")).click();
		driver.findElement(By.className("slds-icon-waffle")).click();
			//3. Click view All and click Sales from App Launcher
		driver.findElement(By.xpath("//button[@class='slds-button']")).click();
		System.out.println("Good");
		
		  driver.findElement(By.xpath("//input[@type='search'][@placeholder='Search apps or items...']")).sendKeys("sales"); 
		  List<WebElement> menuList =driver.findElements(By.xpath("//a[@class='slds-text-heading_small']/following-sibling::p")); 
		  for(int i=0; i<menuList.size(); i++)
		  {
		  //System.out.println(menuList.get(i).getAttribute("title")); 
		  if(menuList.get(i).getAttribute("title").contains("Manage your sales process with accounts, leads, opportunities"))
		  {
			  menuList.get(i).click();  
		  }
		  
		  }
		 
			//4. Click on Cases tab 
		   driver.findElement(By.xpath("//a/span[@class='slds-p-right_small']")).click();
		   List<WebElement> mainMenuList= driver.findElements(By.xpath("//a[@role='menuitem']/span/span"));
		   for(int i=0;i<mainMenuList.size();i++)
		   {
			   System.out.println(mainMenuList.get(i).getText());
			   if(mainMenuList.get(i).getText().equalsIgnoreCase("Cases"))
			   {
				   
				   //wait.until(ExpectedConditions.elementToBeClickable(mainMenuList.get(i)));
				  // mainMenuList.get(i).sendKeys(Keys.ENTER);
				   executor.executeScript("arguments[0].click();", mainMenuList.get(i));
				   System.out.println("Cases menu clicked");
				   break;
			   }
		   }
			//5. Click on the Dropdown icon and select Delete from the case you created by reffering ""case owner alias""
		   executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@role='button']/span[@class='slds-icon_container slds-icon-utility-down']")));
		   System.out.println("dropdown clicked");
		   executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@title='Delete']/div")));
		   System.out.println("Delete clicked");
		   executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//button/span[@dir='ltr'][text()='Delete']")));
		   System.out.println("Delete dialoug clicked");
		 
		   
			//6. Verify the case with your name is deleted or not
			//Expected Result: The case with your name should not be there in the application
		   System.out.println(driver.findElement(By.xpath("//div[@role='alert']/div/div/span[@data-aura-class='forceActionsText']")).getText());
		   WebElement message = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
			wait.until(ExpectedConditions.visibilityOf(message));
			
			if(message.getText().contains("was deleted"))
			{
				System.out.println(message.getText());
				System.out.println("Record deleted successfully");
				//Case "00001512" was deleted
			}

	}

}
