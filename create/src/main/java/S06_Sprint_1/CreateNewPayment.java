package S06_Sprint_1;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateNewPayment {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions co=new ChromeOptions();
		co.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(co);
		WebDriverWait wait= new WebDriverWait(driver, Duration.ofSeconds(15));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		Actions action=new Actions(driver);
		driver.manage().window().maximize();
		
		//1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
	
		//2. Click on the toggle menu button from the left corner
	js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")));
	System.out.println("Toggle menu clicked");
		
		//3. Click View All and click Dashboards from App Launcher
	js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//button[@class='slds-button']")));
	System.out.println("View All menu clicked");
	Thread.sleep(5000);
	js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span/p[text()='Payments']")));
	System.out.println("Payements menu clicked");
	
	
	//Click New
	js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//div[@title='New']")));
	System.out.println("New button clicked");
	
	
	//enter the amount
	driver.findElement(By.xpath("//div[@class='listContent']//a[@role='option']/div/div")).click();
	System.out.println("Amount field clicked");
	driver.findElement(By.xpath("//label/span[@title='required']/following::input[@class='input uiInputSmartNumber']")).sendKeys("500");
	System.out.println("amount entered");
	
	//Update the Status
	driver.findElement(By.xpath("(//span[text()='Status']/following::div/div/div/div/a[@role='button'][@class='select'])[1]")).click();
	System.out.println("Status field clicked");
	driver.findElement(By.xpath("//div[@role='menu']/ul/li/a[@title='Failed']")).click();
	System.out.println("Failed clicked");
	
	//Update the Type
		driver.findElement(By.xpath("(//span[text()='Status']/following::div/div/div/div/a[@role='button'][@class='select'])[2]")).click();
		System.out.println("Type field clicked");
		driver.findElement(By.xpath("//div[@role='menu']/ul/li/a[@title='Capture']")).click();
		System.out.println("Capture clicked");
	
	//External the Type
				driver.findElement(By.xpath("(//span[text()='Status']/following::div/div/div/div/a[@role='button'][@class='select'])[3]")).click();
				System.out.println("Type field clicked");
				driver.findElement(By.xpath("//div[@role='menu']/ul/li/a[@title='External']")).click();
				System.out.println("External  clicked");
				
	//Press the save button
				driver.findElement(By.xpath("(//button/span[text()='Save'])[2]")).click();
				System.out.println("save button clicked");
	//verify the message
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']"))));
				System.out.println(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText());
		
	}

}
