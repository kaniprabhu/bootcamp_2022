package S06_Sprint_1;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditDashboard {

	public static void main(String[] args) throws InterruptedException {
		
			WebDriverManager.chromedriver().setup();
			ChromeOptions co = new ChromeOptions();
			co.addArguments("--disable-notifications");
			WebDriver driver=new ChromeDriver(co);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
			driver.manage().window().maximize();
			JavascriptExecutor js=(JavascriptExecutor)driver;
			Actions action =new Actions(driver);
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
			
			//1. Login to https://login.salesforce.com
			driver.get("https://login.salesforce.com/");
			driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
			driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
			driver.findElement(By.id("Login")).click();
			
			//2. Click on the toggle menu button from the left corner
			js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")));
						
			//3. Click View All and click Dashboards from App Launcher
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@class='slds-button']"))));
			js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//button[@class='slds-button']")));
			
			//4. Click on the Dashboards tab 
			driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("dashboards");
			js.executeScript("arguments[0].click()", driver.findElement(By.xpath("//slot//p[@class='slds-truncate']/mark[text()='Dashboards']")));
			
			//5. Search the Dashboard 'Salesforce Automation by Your Name'
			//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@placeholder='Search recent dashboards..."))));
			Thread.sleep(6000);
			driver.findElement(By.xpath("//input[@placeholder='Search recent dashboards...']")).sendKeys("KANI"+Keys.ENTER);
			
			//6. Click on the Dropdown icon and Select Edit
			Thread.sleep(6000);
			List<WebElement> namelist = driver.findElements(By.xpath("//table[@lightning-datatable_table]/tbody/tr/th[@data-label='Dashboard Name']//lightning-formatted-url/a"));
			for(int i=1; i<=namelist.size();i++)
			{
				System.out.println(namelist.get(i).getAttribute("title"));
				if(namelist.get(i).getAttribute("title").equalsIgnoreCase("Salesforce Automation by KANI PRABHU S"))
				{
					action.moveToElement(driver.findElement(By.xpath("//table[@lightning-datatable_table]//tbody/tr[2]//td[6]//div"))).click().build().perform();
					action.moveToElement(driver.findElement(By.xpath("//a[@role='menuitem']/span[text()='Edit']"))).click().build().perform();
					break;
				}
				
				
			}
			
			//7.Click on the Edit Dashboard Properties icon
			List<WebElement> frameList= driver.findElements(By.tagName("iframe"));
			for(int i=0;i<frameList.size();i++)
			{
			  System.out.println(frameList.get(i).getAttribute("title"));
				if(frameList.get(i).getAttribute("title").equalsIgnoreCase("dashboard"))
				{
					//WebElement temp = frameList.get(i);
					driver.switchTo().frame(i);
					break;
				}
			}
			//driver.switchTo().frame(driver.findElement(By.xpath("iframe[title='dashboard']")));
	action.moveToElement(driver.findElement(By.xpath("//button[@title='Edit Dashboard Properties']"))).click().build().perform();
			//8. Enter Description as 'SalesForce' and click on save.
	Thread.sleep(5000);
	 //driver.findElement(By.id("dashboardDescriptionInput")).sendKeys("Sales");
	driver.findElement(By.xpath("//div[@class='slds-form-element__control']/input[@id='dashboardDescriptionInput']")).sendKeys("Sales");
			//9. Click on Done and  Click on save in the popup window displayed.
	//action.moveToElement(driver.findElement(By.xpath("//button[@id='submitBtn']"))).click().build().perform();
			//10. Verify the Description.
	
			//Expected Result:The Dashboard is Edited Successfully

	}

}
