package S06_Sprint_1;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateNewCase {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--disable-notifications");		
		WebDriver driver =new ChromeDriver(co);
		WebDriverWait wait =new WebDriverWait(driver, Duration.ofSeconds(2));
		//RemoteWebDriver driver = new ChromeDriver();
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
	//1) Launch the app
		driver.get("https://www.salesforce.com/");
				
	//2) Click Login
		driver.findElement(By.xpath("//div[@class='dropdown']")).click();
		
	//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).sendKeys(Keys.ENTER);
		
	//4) Click on Global Actions SVG icon
		driver.findElement(By.className("slds-icon-waffle")).click();
		driver.findElement(By.xpath("//button[@type='button'][text()='View All']")).click();
		driver.findElement(By.xpath("//input[@type='search'][@placeholder='Search apps or items...']")).sendKeys("cases");
		driver.findElement(By.xpath("//lightning-formatted-rich-text[@class='slds-rich-text-editor__output']/span/p/mark[text()='Cases']")).click();
		System.out.println("SVG icon clicked");
		
	//5) Click on New Case
		driver.findElement(By.xpath("//div[@title='New']")).click();
		System.out.println("New Case clicked ");
		
	//6) Choose Contact Name from the dropdown
		driver.findElement(By.xpath("//div/input[@placeholder='Search Contacts...']")).click();
		driver.findElement(By.xpath("(//div[@class='listContent']/ul/li)[1]")).click();
		System.out.println("Name clicked ");
		
	//7)  Select Case origin as email
		
		driver.findElement(By.xpath("(//a[@role='button'][@class='select'])[2]")).click();
		List<WebElement> caseoriginlist =driver.findElements(By.xpath("//div[@role='menu'][@class='select-options']/ul/li/a"));
		for(int i=0; i<caseoriginlist.size(); i++)
		{
			//System.out.println(caseoriginlist.get(i).getAttribute("title"));
			if(caseoriginlist.get(i).getAttribute("title").equalsIgnoreCase("Email"))
			{
				caseoriginlist.get(i).click();
				System.out.println("origin as email clicked");
				break;
			}
		}
		
	//8) Select status as Escalated
		Thread.sleep(2000);
		executor.executeScript("arguments[0].click()", driver.findElement(By.xpath("//label[@class='slds-form-element__label']/following::div/lightning-base-combobox/div")));
		System.out.println("new dropdown clicked");
		//executor.executeScript("arguments[0].click()", driver.findElement(By.xpath("//label[@class='slds-form-element__label']/following::div/lightning-base-combobox/div/div/button/span[text()='Escalated']")));
		List<WebElement> statuslist = driver.findElements(By.xpath("//label[@class='slds-form-element__label']/following::div/lightning-base-combobox/div/div/lightning-base-combobox-item"));
		for(int i=0; i<statuslist.size(); i++)
		{
			//System.out.println(caseoriginlist.get(i).getAttribute("title"));
			if(statuslist.get(i).getAttribute("data-value").equalsIgnoreCase("Escalated"))
			{
				statuslist.get(i).click();
				System.out.println("Escalated clicked");
				break;
			}
		}
		
		
	
	
	
	//9) Enter Subject as 'Testing' and description as 'Dummy'
	driver.findElement(By.xpath("//label/span[text()='Subject']/following::input[@class=' input']")).sendKeys("Test123");
	driver.findElement(By.xpath("//label/span[text()='Description']/following::textarea")).sendKeys("Dummy");
	
	//10) Click 'Save' and verify the message
	Thread.sleep(2000);
	WebElement savebutton = driver.findElement(By.xpath("(//button/span[@dir='ltr'][text()='Save'])[2]"));
	executor.executeScript("arguments[0].click()", savebutton);
	//Expected Result: New Case should be created successfully
	//System.out.println(driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText());
	WebElement message = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
	wait.until(ExpectedConditions.visibilityOf(message));
	
	if(message.getText().contains("was created"))
	{
		System.out.println(message.getText());
		System.out.println("Record created successfully");
	}
	}

}
