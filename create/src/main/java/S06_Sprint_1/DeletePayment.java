package S06_Sprint_1;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeletePayment {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions co=new ChromeOptions();
		co.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(co);
		WebDriverWait wait= new WebDriverWait(driver, Duration.ofSeconds(15));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		Actions action=new Actions(driver);
		driver.manage().window().maximize();
		
		//1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
	
		//2. Click on the toggle menu button from the left corner
	js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")));
	System.out.println("Toggle menu clicked");
		
		//3. Click View All and click Dashboards from App Launcher
	js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//button[@class='slds-button']")));
	System.out.println("View All menu clicked");
	Thread.sleep(5000);
	js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span/p[text()='Payments']")));
	System.out.println("Payements menu clicked");

	driver.findElement(By.xpath("//div[@data-aura-class='forceVirtualAction']")).click();
	System.out.println("Drop down clicked");
	driver.findElement(By.xpath("//ul[@role='presentation']/li/a[@title='Delete']")).click();
	System.out.println("in the Drop down delete clicked");
	driver.findElement(By.xpath("//button/span[text()='Delete']")).click();
	
	}

}
