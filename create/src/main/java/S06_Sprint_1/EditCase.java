package S06_Sprint_1;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditCase {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions co=new ChromeOptions();
		co.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(co);
		 JavascriptExecutor executor = (JavascriptExecutor)driver;
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4000));
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(2));
		//1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
		//2. Click on toggle menu button from the left corner
		driver.findElement(By.className("slds-icon-waffle")).click();
		
		//3. Click view All and click Sales from App Launcher
		driver.findElement(By.xpath("//button[@class='slds-button']")).click();
		driver.findElement(By.xpath("//input[@class='slds-input']")).sendKeys("sales");
		List<WebElement> appLaunlist =	driver.findElements(By.xpath("//div[@class='slds-accordion__content']/slot/div/one-app-launcher-app-tile"));
		for(int i=0;i<appLaunlist.size();i++)
		{
			//System.out.println(appLaunlist.get(i).getText());
			if(appLaunlist.get(i).getText().contains("Manage your sales process with accounts, leads, opportunities, and"))
			{
				appLaunlist.get(i).click();
				break;
			}
		}
		
		//4. Click on Cases tab visible or select from more.
		
		driver.findElement(By.xpath("//a[contains(@class, 'slds-button')]/span[text()='More']")).click();
		List<WebElement> menuList =	driver.findElements(By.xpath("//slot/one-app-nav-bar-menu-item/a/span/span"));
		Thread.sleep(5000);
		//driver.findElement(By.xpath("//a[@role='menuitem']/span/span[text()='Cases']")).click();
		
		for(int i=0;i<menuList.size();i++)
		{
			
			//System.out.println(menuList.get(i).getText());
			
			 if(menuList.get(i).getText().equalsIgnoreCase("Cases"))
			 {
			executor.executeScript("arguments[0].click();", menuList.get(i)); 
			System.out.println("Cases menu clicked");
			 break;
			 }
			 
		}
		
		//5. Click on the Dropdown icon and select Edit from the case you created by reffering "case owner alias"
		
		WebElement dropdown = driver.findElement(By.xpath("//a[@role='button']/span/span/following-sibling::span"));
		executor.executeScript("arguments[0].click();",dropdown); 
		
		driver.findElement(By.xpath("//a[@title='Edit']")).click();
		
		/*
		 * List<WebElement> caseNumlist =
		 * driver.findElements(By.xpath("//table[@role='grid']/tbody/tr"));
		 * List<WebElement> caseNumlist1=new ArrayList<WebElement>(); for(int i = 0;
		 * i<caseNumlist.size(); i++) {
		 * System.out.println(caseNumlist.get(i).getText().equalsIgnoreCase("00001468"))
		 * ; driver.findElements(By.xpath("//table[@role='grid']/tbody/tr"))
		 * //caseNumlist.get(i).getText().equalsIgnoreCase("00001468"); }
		 */
	//6. Update Status as Working
		
		Thread.sleep(2000);
		executor.executeScript("arguments[0].click()", driver.findElement(By.xpath("//label[@class='slds-form-element__label']/following::div/lightning-base-combobox/div")));
		System.out.println("status clicked");
		//List<WebElement> statuslist = driver.findElements(By.xpath("//label[@class='slds-form-element__label']/following::div/lightning-base-combobox/div/div/button"));
		List<WebElement> statuslist = driver.findElements(By.xpath("//label[@class='slds-form-element__label']/following::div/lightning-base-combobox/div/div/lightning-base-combobox-item"));		
		for (int i =0; i<statuslist.size(); i++)
		{
			if(statuslist.get(i).getAttribute("data-value").contains("Working"))
			{
				statuslist.get(i).click();
				System.out.println("working clicked");
				break;
			}
 }
	 
		
		//7. Update Priority to low
 	driver.findElement(By.xpath("//span[text()='Priority']/ancestor::span/following-sibling::div")).click();
 	System.out.println("Priority clicked");
 	List<WebElement> priorityList =driver.findElements(By.xpath("//div[@role='menu']/ul/li/a"));
 	for(int i =0;i<priorityList.size();i++)
 	{
 		//System.out.println(priorityList.get(i).getAttribute("title"));
 		if(priorityList.get(i).getAttribute("title").equalsIgnoreCase("Low"))
 		{
 			priorityList.get(i).click();
 			System.out.println("Priority LOW clicked");
 			break;
 		}
 		
 	}
 	
		//8. Update Case Origin as Phone
 	driver.findElement(By.xpath("//span[text()='Case Origin']/ancestor::span/following-sibling::div")).click();
 	System.out.println("Case Origin clicked");
 	List<WebElement> caseOriginList =driver.findElements(By.xpath("//div[@role='menu']/ul/li/a"));
 	for(int i =0;i<caseOriginList.size();i++)
 	{
 		//System.out.println(caseOriginList.get(i).getAttribute("title"));
 		if(caseOriginList.get(i).getAttribute("title").equalsIgnoreCase("Phone"))
 		{
 			caseOriginList.get(i).click();
 			System.out.println("Case Origin Phone clicked");
 			break;
 		}
 		
 	}
 	
		//9. Update SLA violation to No
 	
 	driver.findElement(By.xpath("//span[text()='SLA Violation']/ancestor::span/following-sibling::div")).click();
 	System.out.println("SLA clicked");
 	List<WebElement> slaViolationList = driver.findElements(By.xpath("//div[@role='menu']/ul/li/a"));
 	for(int i =0;i<slaViolationList.size();i++)
 	{
 		//System.out.println(slaViolationList.get(i).getAttribute("title"));
 		if(slaViolationList.get(i).getAttribute("title").equalsIgnoreCase("No"))
 		{
 			slaViolationList.get(i).click();
 			System.out.println("SLA No clicked");
 			break;
 		}
 		
 	}
		//10. Click on Save and Verify Status as WorkingExpected result:Case is Edited Successfully  and Status is working
 		driver.findElement(By.xpath("//button[@type='button'][@title='Save']")).click();
 		
 		WebElement message = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
 		wait.until(ExpectedConditions.visibilityOf(message));
 		if(message.getText().contains("was saved."))
 		{
 			System.out.println("Record updated successfully");
 			System.out.println(message.getText());
 			//Case "00001512" was saved..
 		}

	}

}
