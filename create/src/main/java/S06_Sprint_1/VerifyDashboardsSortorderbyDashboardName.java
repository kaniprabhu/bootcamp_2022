package S06_Sprint_1;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class VerifyDashboardsSortorderbyDashboardName {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions co=new ChromeOptions();
		co.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(co);
		WebDriverWait wait= new WebDriverWait(driver, Duration.ofSeconds(10));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		Actions action=new Actions(driver);
		driver.manage().window().maximize();
		
			//1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
			//2. Click on the toggle menu button from the left corner
		js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")));
		
			//3. Click View All and click Dashboards from App Launcher
		js.executeScript("arguments[0].click();",driver.findElement(By.xpath("//button[@class='slds-button']")));
		
			//4. Click on the Dashboards tab 
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Dashboa");
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy((By.xpath("//one-app-launcher-tab-item/a"))));
		List<WebElement> svglist =	driver.findElements(By.xpath("//one-app-launcher-tab-item/a"));
		for(int i=0; i<svglist.size();i++)
		{
			if(svglist.get(i).getAttribute("data-label").equalsIgnoreCase("Dashboards"))
			{
				action.moveToElement(svglist.get(i)).click().build().perform();
			}
		}
		
		List<String> originalList = new ArrayList<String>();
		List<WebElement> originalEleList = driver.findElements(By.xpath("//tbody[@lightning-datatable_table]/tr/th//lightning-formatted-url/a"));
		for(int i=0; i<originalEleList.size();i++)
		{
			originalList.add(originalEleList.get(i).getAttribute("title"));
			
		}
		System.out.println("before Sort " + originalList);
		//Collections.sort(originalList,Collections.reverseOrder());
		//List<Object> originalList = originalList.stream().sorted().collect(Collectors.toList());
		Collections.sort(originalList);
		List<String> originaupdatedlList = new LinkedList<String>();
		originaupdatedlList.addAll(originalList);
		
		//originalList = originalList.stream().sorted().collect(Collectors.toList());
		System.out.println("After sort thru collection" + originaupdatedlList);
			//5. Click the sort arrow in the Dashboard Name.
		action.moveToElement(driver.findElement(By.xpath("//lightning-primitive-icon[@class='slds-icon_container']"))).click().build().perform();
			//6. Verify the Dashboard displayed in ascending order by Dashboard name.
		Thread.sleep(5000);
		List<String> AftersortList = new ArrayList<String>();
		List<WebElement> AftersorteleList = driver.findElements(By.xpath("//tbody[@lightning-datatable_table]/tr/th//lightning-formatted-url/a"));
		for(int i=0; i<AftersorteleList.size();i++)
		{
			AftersortList.add(AftersorteleList.get(i).getAttribute("title"));
			
		}
		System.out.println("After sort thru Arrow "+AftersortList);
		
		//driver.findElements(By.xpath("//tbody[@lightning-datatable_table]/tr/th"));
			//Expected Result: 	Dashboards should be displayed in ascending order by Dashboard name
	}

}
