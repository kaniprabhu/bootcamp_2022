package S06_Sprint_1;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateNewDashboard {

	public static void main(String[] args) throws InterruptedException {
			
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(co);
		Actions action = new Actions(driver);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		
			//1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
			//2. Click on the toggle menu button from the left corner
		executor.executeScript("arguments[0].click();", driver.findElement(By.className("slds-icon-waffle")));
		
			//3. Click View All and click Dashboards from App Launcher
		executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//lightning-button/button[@type='button']")));
				
			//4. Click on the New Dashboard option
		List<WebElement> AppList= driver.findElements(By.xpath("//slot/ul/li/one-app-launcher-tab-item/a/span/lightning-formatted-rich-text/span/p"));
		for(int i=0;i<AppList.size();i++)
		{
			if(AppList.get(i).getText().equalsIgnoreCase("Dashboards"))
			{
				wait.until(ExpectedConditions.elementToBeClickable(AppList.get(i)));
				action.moveToElement(AppList.get(i)).click().build().perform();
				break;
			}
		
		}	
		action.moveToElement(driver.findElement(By.xpath("//div[@title='New Dashboard']"))).click().build().perform();
		//System.out.println("New button clicked");
		
			//5. Enter Name as 'Salesforce Automation by Your Name ' and Click on Create.
		List<WebElement> framelist = driver.findElements(By.tagName("iframe"));
		for(int i=0; i<framelist.size();i++)
		{
			if(framelist.get(i).getAttribute("title").equalsIgnoreCase("Dashboard"))
			{
				Thread.sleep(6000);
				driver.switchTo().frame(i);
				//System.out.println("entered into frame");
			}
			
		}
		
		driver.findElement(By.id("dashboardNameInput")).sendKeys("Salesforce Automation by KANI PRABHU S");
		System.out.println("name entered");
		driver.findElement(By.id("dashboardDescriptionInput")).sendKeys("Thru Automation");
			//6.Click on Save and Verify Dashboard name.
		executor.executeScript("arguments[0].click();",driver.findElement(By.id("submitBtn")));
		//System.out.println("frame save button clicked");
	
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-button-group']/button"))));
		action.moveToElement(driver.findElement(By.xpath("//div[@class='slds-button-group']/button"))).click().build().perform();
		//System.out.println("main content save clicked");
		
		driver.switchTo().defaultContent();
			//Expected Result:The New Dashboard is created Successfully
		String message = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")))).getText();
		if(message.equalsIgnoreCase("Dashboard saved"))
			{
				System.out.println("Dashboard created successfully");
			}

	}

}
